import { apiWrapper } from './ApiFunctions'

class UsersApi {

  static getUsers() {
    const url = 'https://ti-react-test.herokuapp.com/users';
    return apiWrapper('GET', url, {})
  }
}

export default UsersApi;
